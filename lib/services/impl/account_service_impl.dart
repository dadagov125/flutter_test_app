import 'package:hive/hive.dart';
import 'package:test_app/models/user_model.dart';
import 'dart:convert';
import '../account_service.dart';

class AccountServiceImpl extends AccountService {
  static const _currentUserKey = 'CurrentUser';

  static const _allUsersKey = 'AllUsers';

  static const _accountBoxKey = 'AccountBox';

  Future<T> _onBox<T>(Future<T> Function(Box<String?> box) fn) async {
    final box = await Hive.openBox<String?>(_accountBoxKey);
    try {
      return fn(box);
    } finally {
      box.close();
    }
  }

  @override
  Future<UserModel?> currentUser() async {
    return _onBox<UserModel?>((box) async {
      final s = box.get(_currentUserKey);
      if (s == null) return null;
      final map = json.decode(s);
      return UserModel.fromJson(map);
    });
  }

  @override
  Future<UserModel> login(
      {required String login, required String password}) async {
    return _onBox<UserModel>((box) async {
      final s = box.get(_allUsersKey);
      if (s == null) throw Exception('User not fount');
      final list = json.decode(s) as List;
      final userList = list.map((map) => UserModel.fromJson(map)).toList();
      final users = userList
          .where((u) => u.login == login && u.password == password)
          .toList();
      if (users.length == 0) {
        throw Exception('User not fount');
      }
      final user = users[0];
      await box.put(_currentUserKey, json.encode(user.toJson()));
      return user;
    });
  }

  @override
  Future<void> registerUser(
      {required String name,
      required String login,
      required String password}) async {
    await _onBox<void>((box) async {
      List<UserModel> users = [];
      final s = box.get(_allUsersKey);
      if (s != null) {
        final list = json.decode(s) as List;
        users = list.map((map) => UserModel.fromJson(map)).toList();
        if (users.any((u) => u.login == login)) {
          throw Exception('User with login ${login} already registred');
        }
      }

      users.add(UserModel(
          name: name,
          login: login,
          password: password,
          createdAt: DateTime.now()));

      final map = users.map((u) => u.toJson()).toList();
      await box.put(_allUsersKey, json.encode(map));
    });
  }

  @override
  Future<void> logout() async {
    return _onBox((box) => box.delete(_currentUserKey));
  }
}
