import 'package:dio/dio.dart';
import 'package:test_app/models/cat_fact_model.dart';
import 'package:test_app/models/pageable_list.dart';
import 'package:test_app/services/cat_fact_service.dart';

class CatFactServiceImpl extends CatFactService {
  CatFactServiceImpl() {
    _init();
  }

  late final Dio dio;

  _init() {
    dio = Dio(BaseOptions(
        baseUrl: 'https://catfact.ninja',
        responseType: ResponseType.json));
  }

  @override
  Future<PageableList<CatFactModel>> getPage({int page = 1}) {
    return dio.get('/facts', queryParameters: {'page': page}).then((response) =>
        PageableList.fromJson(
            response.data, (dynamic json) => CatFactModel.fromJson(json)));
  }
}
