import 'package:test_app/models/user_model.dart';

abstract class AccountService {
  Future<void> registerUser({required String name, required String login,required String password});

  Future<UserModel> login({required String login, required String password});

  Future<UserModel?> currentUser();

  Future<void> logout();

}
