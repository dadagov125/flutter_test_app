import 'package:test_app/models/cat_fact_model.dart';
import 'package:test_app/models/pageable_list.dart';

abstract class CatFactService {
  Future<PageableList<CatFactModel>> getPage({int page = 1});
}
