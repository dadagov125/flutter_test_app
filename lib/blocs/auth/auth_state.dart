import 'package:test_app/models/user_model.dart';
import 'package:equatable/equatable.dart';

class AuthState with EquatableMixin {
  AuthState({this.currentUser, required this.isLoading});

  bool get isLoggedIn {
    return currentUser != null;
  }

  final bool isLoading;

  final UserModel? currentUser;

  AuthState copyWith({UserModel? currentUser, bool? isLoading}) {
    return AuthState(
        currentUser: currentUser ?? this.currentUser,
        isLoading: isLoading ?? this.isLoading);
  }

  @override
  List<Object?> get props => [isLoading, currentUser];
}
