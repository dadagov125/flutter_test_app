import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:test_app/services/account_service.dart';

import 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthState(isLoading: false));
  final AccountService _accountService = GetIt.I.get();

  Future<void> login({required String login, required String password}) async {
    emit(state.copyWith(isLoading: true));
    try {
      final currentUser =
          await _accountService.login(login: login, password: password);
      emit(state.copyWith(currentUser: currentUser, isLoading: false));
    } catch (err) {
      emit(state.copyWith(isLoading: false));
      throw err;
    }
  }

  Future<void> checkAuth() async {
    final currentUser = await _accountService.currentUser();
    emit(state.copyWith(currentUser: currentUser));
  }

  Future<void> register(
      {required String name,
      required String login,
      required String password}) async {
    emit(state.copyWith(isLoading: true));
    try {
      await _accountService.registerUser(
          name: name, login: login, password: password);
      emit(state.copyWith(isLoading: false));
    } catch (err) {
      emit(state.copyWith(isLoading: false));
      throw err;
    }
  }

  Future<void> logout() async {
    emit(state.copyWith(isLoading: true));
    try {
      await _accountService.logout();
      emit(AuthState(isLoading: false, currentUser: null));
    } catch (err) {
      emit(state.copyWith(isLoading: false));
      throw err;
    }
  }
}
