import 'package:equatable/equatable.dart';
import 'package:test_app/models/cat_fact_model.dart';

class CatFactState with EquatableMixin {
  CatFactState(
      {required this.facts,
      required this.currentPage,
      required this.lastPage,
      required this.total,
      this.isLoading = false});

  final List<CatFactModel> facts;
  final int currentPage;
  final int lastPage;
  final int total;
  final bool isLoading;

  CatFactState copyWith(
      {List<CatFactModel>? facts,
      int? currentPage,
      int? lastPage,
      int? total,
      bool? isLoading}) {
    return CatFactState(
        facts: facts ?? this.facts,
        currentPage: currentPage ?? this.currentPage,
        lastPage: lastPage ?? this.lastPage,
        total: total ?? this.total,
        isLoading: isLoading ?? this.isLoading);
  }

  @override
  List<Object?> get props => [currentPage, lastPage, total, isLoading, facts];
}
