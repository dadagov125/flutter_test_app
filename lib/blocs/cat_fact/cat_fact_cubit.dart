import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:test_app/services/cat_fact_service.dart';

import 'cat_fact_state.dart';

class CatFactCubit extends Cubit<CatFactState> {
  CatFactCubit()
      : super(CatFactState(facts: [], total: 0, currentPage: 0, lastPage: 0));
  final CatFactService _catFactService = GetIt.I.get();

  Future<void> load() async {
    if (state.currentPage > 0 && state.currentPage == state.lastPage) return;
    emit(state.copyWith(isLoading: true));
    try {
      final page = await _catFactService.getPage(page: 1 + state.currentPage);
      final newState = state.copyWith(
          facts: [...state.facts, ...page.data],
          currentPage: page.currentPage,
          total: page.total,
          lastPage: page.lastPage,
          isLoading: false);
      emit(newState);
    } catch (err) {
      emit(state.copyWith(isLoading: false));
      throw err;
    }
  }
}
