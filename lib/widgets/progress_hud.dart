import 'package:flutter/material.dart';

class ProgressHud extends StatelessWidget {
  const ProgressHud({Key? key, required this.isLoading, required this.child})
      : super(key: key);
  final bool isLoading;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        isLoading
            ? Center(
                child: new CircularProgressIndicator(),
              )
            : Container()
      ],
    );
  }
}
