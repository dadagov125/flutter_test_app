import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/blocs/cat_fact/cat_fact_cubit.dart';
import 'package:test_app/blocs/cat_fact/cat_fact_state.dart';

class CatFactList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CatFactListState();
}

class _CatFactListState extends State<CatFactList> {
  @override
  void initState() {
    super.initState();
    _loadFacts();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CatFactCubit, CatFactState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(),
          body: Column(
            children: [
              Visibility(
                  visible: state.facts.length > 0,
                  child: Flexible(
                      child: NotificationListener<ScrollNotification>(
                    onNotification: (scroll) {
                      if (!state.isLoading &&
                          scroll.metrics.pixels ==
                              scroll.metrics.maxScrollExtent) {
                        _loadFacts();
                      }
                      return false;
                    },
                    child: ListView.separated(
                      separatorBuilder: (_, __) => Divider(),
                      itemCount: state.facts.length,
                      itemBuilder: (context, int index) {
                        final fact = state.facts[index];
                        return ListTile(title: Text(fact.fact));
                      },
                    ),
                  ))),
              Visibility(
                  visible: state.isLoading,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ))
            ],
          ),
        );
      },
    );
  }

  void _loadFacts() {
    final cubit = context.read<CatFactCubit>();
    cubit.load().catchError((err) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(err.toString())));
    });
  }
}
