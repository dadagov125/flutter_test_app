import 'package:get_it/get_it.dart';
import 'package:test_app/services/account_service.dart';
import 'package:test_app/services/cat_fact_service.dart';
import 'package:test_app/services/impl/account_service_impl.dart';
import 'package:test_app/services/impl/cat_fact_service.dart';

class AppLocator {
  static void init() {
    final getIt = GetIt.I;

    getIt.registerSingleton<AccountService>(AccountServiceImpl());
    getIt.registerSingleton<CatFactService>(CatFactServiceImpl());
  }

  static Future<void> initAsync() {
    init();
    return GetIt.I.allReady();
  }
}
