import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cat_fact_model.g.dart';

@JsonSerializable(
    explicitToJson: true, genericArgumentFactories: true, anyMap: true)
class CatFactModel with EquatableMixin {
  CatFactModel(this.fact);

  final String fact;

  factory CatFactModel.fromJson(Map<String, dynamic> json) => _$CatFactModelFromJson(json);

  Map<String, dynamic> toJson() => _$CatFactModelToJson(this);

  @override
  List<Object?> get props => [fact];
}