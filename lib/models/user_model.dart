import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable(
    explicitToJson: true, genericArgumentFactories: true, anyMap: true)
class UserModel with EquatableMixin {
  UserModel(
      {required this.name,
      required this.login,
      required this.password,
      required this.createdAt});

  final String name;
  final String login;
  final String password;
  final DateTime createdAt;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  @override
  List<Object?> get props => [name, login, password, createdAt];
}
