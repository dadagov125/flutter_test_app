// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pageable_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PageableList<T> _$PageableListFromJson<T>(
  Map json,
  T Function(Object? json) fromJsonT,
) =>
    PageableList<T>(
      data: (json['data'] as List<dynamic>).map(fromJsonT).toList(),
      currentPage: json['current_page'] as int,
      lastPage: json['last_page'] as int,
      total: json['total'] as int,
    );

Map<String, dynamic> _$PageableListToJson<T>(
  PageableList<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'data': instance.data.map(toJsonT).toList(),
      'current_page': instance.currentPage,
      'last_page': instance.lastPage,
      'total': instance.total,
    };
