import 'package:json_annotation/json_annotation.dart';

part 'pageable_list.g.dart';

@JsonSerializable(
    explicitToJson: true, genericArgumentFactories: true, anyMap: true)
class PageableList<T> {
  PageableList(
      {required this.data,
      required this.currentPage,
      required this.lastPage,
      required this.total});

  final List<T> data;
  @JsonKey(name: 'current_page')
  final int currentPage;
  @JsonKey(name: 'last_page')
  final int lastPage;
  final int total;


  factory PageableList.fromJson(
          Map<String, dynamic> json, T Function(dynamic json) fromJsonT) =>
      _$PageableListFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$PageableListToJson(this, toJsonT);
}
