import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/screens/home_screen.dart';
import 'package:test_app/screens/login_screen.dart';

import 'blocs/auth/auth_cubit.dart';
import 'blocs/auth/auth_state.dart';
import 'blocs/cat_fact/cat_fact_cubit.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AuthCubit(),
      child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: BlocBuilder<AuthCubit, AuthState>(
            builder: (context, state) {
              if (state.isLoggedIn)
                return BlocProvider(
                    create: (_) => CatFactCubit(), child: const HomeScreen());

              return Navigator(
                onGenerateRoute: (settings) {
                  return MaterialPageRoute(builder: (_) {
                    return LoginScreen();
                  });
                },
              );
            },
          )),
    );
  }
}
