import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:test_app/di/app_locator.dart';

import 'app.dart';


void main() async {
  await Hive.initFlutter();
  await AppLocator.initAsync();
  runApp(const MyApp());
}
