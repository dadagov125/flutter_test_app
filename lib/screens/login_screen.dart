import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/blocs/auth/auth_cubit.dart';
import 'package:test_app/blocs/auth/auth_state.dart';
import 'package:test_app/screens/register_screen.dart';
import 'package:test_app/utils/validators.dart';
import 'package:test_app/widgets/progress_hud.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late final TextEditingController _loginCtrl;
  late final TextEditingController _passwordCtrl;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    context.read<AuthCubit>().checkAuth().catchError(_handleError);
    _loginCtrl = TextEditingController();
    _passwordCtrl = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        return ProgressHud(
          isLoading: state.isLoading,
          child: Scaffold(
            appBar: AppBar(title: Text('Login')),
            body: Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _loginCtrl,
                        decoration: InputDecoration(label: Text('Login')),
                        validator: reqiredValidator,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _passwordCtrl,
                        decoration: InputDecoration(label: Text('Password')),
                        validator: reqiredValidator,
                      ),
                      SizedBox(height: 20),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: InkWell(
                          onTap: _toRegisterScree,
                          child: Text(
                            'Create account',
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    final authCubit = context.read<AuthCubit>();
                    authCubit
                        .login(
                            login: _loginCtrl.text,
                            password: _passwordCtrl.text)
                        .catchError(_handleError);
                  }
                },
                child: Text('Login'),
              ),
            ),
          ),
        );
      },
    );
  }

  _handleError(err) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(err.toString())));
  }

  void _toRegisterScree() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (_) => RegisterScreen()));
  }

  @override
  void dispose() {
    super.dispose();
    _loginCtrl.dispose();
    _passwordCtrl.dispose();
  }
}
