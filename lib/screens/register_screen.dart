import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/blocs/auth/auth_cubit.dart';
import 'package:test_app/blocs/auth/auth_state.dart';
import 'package:test_app/screens/login_screen.dart';
import 'package:test_app/utils/validators.dart';
import 'package:test_app/widgets/progress_hud.dart';

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late final TextEditingController _loginCtrl;
  late final TextEditingController _passwordCtrl;
  late final TextEditingController _nameCtrl;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _loginCtrl = TextEditingController();
    _passwordCtrl = TextEditingController();
    _nameCtrl = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        return ProgressHud(
          isLoading: state.isLoading,
          child: Scaffold(
            appBar: AppBar(title: Text('Register')),
            body: Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _nameCtrl,
                        decoration: InputDecoration(label: Text('Name')),
                        validator: reqiredValidator,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _loginCtrl,
                        decoration: InputDecoration(label: Text('Login')),
                        validator: reqiredValidator,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _passwordCtrl,
                        decoration: InputDecoration(label: Text('Password')),
                        validator: reqiredValidator,
                      ),
                      SizedBox(height: 20),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: InkWell(
                          onTap: _toLoginScreen,
                          child: Text(
                            'I have account',
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    final authCubit = context.read<AuthCubit>();
                    authCubit
                        .register(
                            name: _nameCtrl.text,
                            login: _loginCtrl.text,
                            password: _passwordCtrl.text)
                        .then((value) => _toLoginScreen())
                        .catchError(_handleError);
                  }
                },
                child: Text('Register'),
              ),
            ),
          ),
        );
      },
    );
  }

  void _toLoginScreen() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (_) => LoginScreen()));
  }



  _handleError(err) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(err.toString())));
  }

  @override
  void dispose() {
    super.dispose();
    _loginCtrl.dispose();
    _passwordCtrl.dispose();
    _nameCtrl.dispose();
  }
}
